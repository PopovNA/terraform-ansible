# Terraform in CI/CD

This project utilizes GitLab CI/CD to run Terraform to provision multiple AWS EC2 instances and Ansible to configure those instances. The Terraform state is handled by the [Terraform Cloud service](https://cloud.hashicorp.com/products/terraform) for which the CI/CD pipeline authenticates against. Once authenticated, `terraform-apply` is run to provision the new EC2 instances. The instances are configured by running an Ansible-Playbook in the CI/CD pipeline. 

## Indepth Explanation

The CI/CD pipeline script [.gitlab-ci.yml](https://gitlab.com/jamesearl341/terraform/-/blob/main/.gitlab-ci.yml) has four stages, a `before_script_template` named `&terraform_login` to handle the Terraform login and another `before_script_template` name `install_aws_ssh_keys` to handle the SSH Key installation. The before_script_template is run when Terraform requires access to that Terraform statefile that is being hosted in the Terraform Cloud. This script creates the `~/.terraform.d/credentials.tfrc.json` file and populates it with the Gitlab `$TFC_TOKEN` secret. `terraform init` is then run to complete the login.   


The four pipeline stages are:
- terraform_apply
- python_generate_ansible_hosts
- ansible_provision_hosts
- terraform_destory


The Terraform [main.tf](https://gitlab.com/jamesearl341/terraform/-/blob/main/aws-instance/main.tf) contains three resource groups, `aws_instance: controller_node`, `aws_instance: worker_node` and `aws_security_group: web-sg`. Both the `aws_instances` deploy the same AWS `instance_type` or `t2.micro`, but what sets these apart is with the AWS tags that are applied. The `controller_node` receives the tag `ansible_host_group = "controller"` and the `worker_node` receive the `ansible_host_group = "worker"` tag. These tags allow us to differentiate the instances; the instance with the `controller` tag will become the Kubernetes master server, whereas the instances with the `worker` tag will become Kubernetes Nodes.

Notes
* SSH Keys are managed by AWS and deployed to the EC2 instances on creation (via AWS)
* The pipeline stages `terraform-apply`, `python_generate_ansible_hosts` and `ansible_provision_hosts` are only run if the Gitlab secret/environmental `$RUN_TYPE` is `terraform_deploy`. Similarly, the pipeline stage `terraform_destroy`is only run if the `$RUN_TYPE` environmental is `terraform_destory`. This is to ensure that when we're deploying, the stage for `terraform_destory` is not also run, and visa-verse. 

### terraform_apply

Once the `*terraform_login` `before_script_template` has been run for this stage, we run the `terraform validate` command to confirm that our Terraform configuration is correct, and then `terraform apply -auto-approve` to apply the Terraform configuration specified in the `/aws-instance/main.tf` file. 

### python_generate_ansible_hosts

This stage also requires the `*terraform_login` script to be run, and once done, the Python3 script [ansible_hosts_from_tf.py
](https://gitlab.com/jamesearl341/terraform/-/blob/main/aws-instance/scripts/ansible_hosts_from_tf.py) is run. This script runs the `terraform state pull` command to get the current terraform state and stores the output in JSON. 

The script then filters the instances for the `ansible_host_group` tag and separates the different tag types into groups. One group for `controller` and another group for `worker`. 

```
for instance in instances:
    if "ansible_host_group" in instance['attributes']['tags']:
        group = instance['attributes']['tags']['ansible_host_group']
        public_ip = instance['attributes']['public_ip']
        public_dns = instance['attributes']['public_dns']

        # Create a new dictionary if we haven't seen this group yet
        if group not in groups:
            groups[group] = {"hosts": {}}
        
    # Store the public IP address to an empty dictionary so it's converted to YAML correctly
    groups[group]['hosts'][public_ip] = None
```

This output is then formatted to YAML and saved as `hosts.yml`. The `hosts.yml` can be accessed as an artifact for the subsequent pipeline stages. The contents of the files looks like this: 

```
all:
  children:
    controller:
      hosts:
        18.130.122.199: null
    worker:
      hosts:
        13.40.194.42: null
        13.40.80.0: null
        3.10.51.151: null
  hosts: null
```

### ansible_provision_hosts

This is the part where Ansible takes over with provisioning the software we need on the AWS instances. Before doing that, the CI/CD Pipeline uses the `before_script_template` `install_aws_ssh_keys` to start the SSH-Agent service and to install the SSH Key that Ansible will use to communicate with the AWS instances. 

The `before_script_template` looks like this: 
```
.before_script_template: &install_aws_ssh_keys
  before_script:
    # If `ssh-agent` is not present, update apt and install openssh-client
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval `ssh-agent -s`
    
    # Create the ssh directories, set the correct permissions and copy the SSH Key from an environmental
    - mkdir -p /root/.ssh
    - chmod 700 /root/.ssh
    - cp $AWS_RSA /root/.ssh/id_rsa
    - chmod 600 /root/.ssh/id_rsa
    - ssh-add /root/.ssh/id_rsa

    # Ensure Ansible doesn't throw a prompt when SSHing
    - export ANSIBLE_HOST_KEY_CHECKING=False
```

The `hosts.yml` artifact file created in the `python_generate_ansible_hosts` is moved into the `/ansible/` folder and the Ansible-Playbook `get_time.yml` is run. This Playbook is rather simple in that it only gathers the `uptime` of the instance and then outputs that as a debug line. 

To run the Ansible-Playbook
```
  script:
    - mv aws-instance/hosts.yml aws-instance/ansible/
    - cd aws-instance/ansible/
    - ansible-playbook -i hosts.yml playbooks/get_time.yml
```

The Playbook `get_time.yml`
```
- name: Get instance time
  remote_user: ubuntu
  gather_facts: false
  hosts: all
  
  tasks:
    - name: Get time
      command: uptime
      register: uptime

    - name:
      ansible.builtin.debug:
        var: uptime['stdout_lines']
```

### terraform_destroy

Using the methods to authenticate against the Terraform cloud as the `terraform_apply` and `python_generate_ansible_hosts` stages, the `terraform_destroy` stage runs the command `terraform destroy -auto-approve`. This stage is used to unprovision the AWS instances.

## Expected Outcome

For the deployment stages, the Ansible-Playbook `get_time.yml` will output the `uptime` of the configured AWS instances. 

The output will be similar
```
TASK [ansible.builtin.debug] ***************************************************
ok: [3.8.189.75] => {
    "uptime['stdout_lines']": [
        " 18:34:45 up 0 min,  1 user,  load average: 0.43, 0.13, 0.05"
    ]
}
ok: [3.8.151.47] => {
    "uptime['stdout_lines']": [
        " 18:34:45 up 0 min,  1 user,  load average: 0.69, 0.22, 0.08"
    ]
}
ok: [52.56.98.47] => {
    "uptime['stdout_lines']": [
        " 18:34:45 up 0 min,  1 user,  load average: 0.73, 0.20, 0.07"
    ]
}
ok: [35.178.170.175] => {
    "uptime['stdout_lines']": [
        " 18:34:45 up 0 min,  1 user,  load average: 0.43, 0.15, 0.05"
    ]
}
```

For the destory stage, the output will state that all resources have been destoryed. 

```
Destroy complete! Resources: 4 destroyed.
Cleaning up file based variables
Job succeeded
```

# Getting Started

## Prerequisites

This project uses the following Cloud services:

```
AWS for EC2 instances
Terraform Cloud for maintaining the Terraform state file
GitLab for the code respository and CI/CD
```

### AWS

You can create an AWS management account [here](https://aws.amazon.com/console/). Once created, you'll need to grab the "AMI" which can be found in the AMI Catalog [here](https://eu-west-2.console.aws.amazon.com/ec2/v2/home?region=eu-west-2#AMICatalog:).

Configure an IAM user account and generate an AWS access key ID and secret access key by following [these instructions](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-prereqs.html) 

The following environmental variables are required in the Gitlab project
```
AWS_ACCESS_KEY_ID - 
AWS_SECRET_ACCESS_KEY - 
AWS_RSA -

TF_VAR_EC2_AMI - The AMI of the image found earlier
TF_VAR_EC2_INSTANCE_TYPE - The EC2 [instance type](https://eu-west-2.console.aws.amazon.com/ec2/v2/home?region=eu-west-2#InstanceTypes:), t2.micro is recommend
TF_VAR_EC2_REGION - The EC2 region
```

### Terraform

You can create an account for the Terraform Cloud [here](https://cloud.hashicorp.com/products/terraform). Once done, create a new Organization and then a new Workspace. 

Go to Settings -> General settings and set the "Execution Mode" to Local

Create a new API Token [here](https://app.terraform.io/app/settings/tokens)

The following environmental variables are required in the Gitlab project
```
TFC_TOKEN - The API token
TF_CLOUD_ORGANIZATION - The Terraform Organization
TF_WORKSPACE - The Terraform Workspace
```

### Additional Variables

```
RUN_TYPE - Set to "terraform_deploy" as default
```

## Deployment

With the above prerequisites complete, go to CI/CD -> Pipelines on Gitlab, then "Run pipeline" and "Run pipeline" again. The CI/CD Pipeline will start and can be viewed on the CI/CD -> Pipelines page.

## Extra configuration

You can change the number of `worker-nodes` that are created by editing `count = 3` on line 32 of `aws-instance/main.tf` file.

## TODO

Remove the hardcoded `cidr_blocks` configuration from `aws-instance/main.tf`. This should be a variable instead.

# Built With
* [Terraform](https://www.terraform.io/)
* [Python3](https://www.python.org/)
* [Ansible](https://www.ansible.com/)
* [Gitlab](https://gitlab.com)
* [VsCode](https://code.visualstudio.com/)

## Authors

* **James Earl** - [Github](https://github.com/insidus341/Hello) / [GitLab](https://gitlab.com/jamesearl341)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
