terraform {
 cloud {}

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.18"
    }
  }

}

provider "aws" {
  region  = var.EC2_REGION
}

resource "aws_instance" "controller_node" {
  ami           = var.EC2_AMI
  instance_type = var.EC2_INSTANCE_TYPE
  vpc_security_group_ids = [aws_security_group.web-sg.id]
  key_name = "Terraform"

  tags = {
    Name = "Controller Node"
    Service = "Kubernetes"
    Environment = "Development"
    ansible_host_group = "controller"
  }
}

resource "aws_instance" "worker_node" {
  count = 3
  
  ami           = var.EC2_AMI
  instance_type = var.EC2_INSTANCE_TYPE
  vpc_security_group_ids = [aws_security_group.web-sg.id]
  key_name = "Terraform"

  tags = {
    Name = "Worker Node ${count.index}"
    Service = "Kubernetes"
    Environment = "Development"
    ansible_host_group = "worker"
  }
}

resource "aws_security_group" "web-sg" {
  name = "Terraform_SG"
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["195.27.35.158/32", "51.195.217.164/32", "86.14.184.66/32"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
