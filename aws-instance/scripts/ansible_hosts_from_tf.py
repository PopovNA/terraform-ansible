import subprocess
import yaml
import json
import sys

def getStateFromTerraform():
    s = subprocess.Popen("terraform state pull", shell=True, stdout=subprocess.PIPE)
    state = s.stdout.read()

    js = json.loads(state)
    return js

def getHostsFromTerraformState(state):
    groups = {}

    for resource in state['resources']:
        instances = resource['instances']
        name = resource['name']
        type = resource['type']

        if type != "aws_instance":
            continue

        for instance in instances:
            if "ansible_host_group" in instance['attributes']['tags']:
                group = instance['attributes']['tags']['ansible_host_group']
                public_ip = instance['attributes']['public_ip']
                public_dns = instance['attributes']['public_dns']

                # Create a new dictionary if we haven't seen this group yet
                if group not in groups:
                    groups[group] = {"hosts": {}}
                
            # Store the public IP address to an empty dictionary so it's converted to YAML correctly
            groups[group]['hosts'][public_ip] = None
    
    return groups


def formatYaml(groups): 
    hosts = {
        "all": {
            "hosts": None,
            "children": groups
        }
    }  

    return hosts

def writeHostsFile(file, data):
    print("writing to " + file)
    print(data)
    with open(file, 'w+') as file:
        yaml.dump(data, file)


if __name__ == "__main__":
    
    file = "./hosts.yml"

    state = getStateFromTerraform()
    
    groups = getHostsFromTerraformState(state)
    print(groups)

    yamlData = formatYaml(groups)
    print(yamlData)

    writeHostsFile(file, yamlData) 
