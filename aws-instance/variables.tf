variable "EC2_AMI" {
  description = "The EC2 AMI to use"
  type        = string
}

variable "EC2_INSTANCE_TYPE" {
  description = "The EC2 Instance type"
  type        = string
}

variable "EC2_REGION" {
  description = "The EC2 Region"
  type        = string
}
